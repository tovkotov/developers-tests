package count_digits;

import junit.framework.TestCase;

import java.util.Random;
import java.util.TreeMap;

public class CountDigitsTest extends TestCase {

    public void testValueArrayTest() {
        Random random = new Random();
        int n = 100; // размерность массива

        Integer digit = 5; // сгенерируем массив в котором посчитаем количество digit

        Integer exp = 0;

        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            int j = random.nextInt(n);
            if (j == digit) {
                exp++;
            }
            array[i] = j;
        }

        TreeMap<Integer, Integer> counts = CountDigits.fromArray(array);
        if (counts.containsKey(digit))
            assertEquals(exp, counts.get(digit));
    }

}
