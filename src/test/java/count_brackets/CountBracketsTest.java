package count_brackets;

import junit.framework.TestCase;

public class CountBracketsTest extends TestCase {
    String oddValueFalse;// нечетное количество
    String evenValueFalse;// четное неверное расположение
    String evenValueTrue;// четное верное расположение

    @Override
    protected void setUp() {
         oddValueFalse = "(((((";
         evenValueFalse = "((((()";
         evenValueTrue = "(()()()[])";
    }


    public void testOddValueFalse  () {
        assertEquals(false, CountBracket.countBracketsCheck(oddValueFalse));
    }
    public void testEvenValueFalse  () {
        assertEquals(false, CountBracket.countBracketsCheck(evenValueFalse));
    }
    public void testEvenValueTrue  () {
        assertEquals(true, CountBracket.countBracketsCheck(evenValueTrue));
    }
}
