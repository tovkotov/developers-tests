package zero_to_one;

import junit.framework.TestCase;

public class Zero2OneChangerTest extends TestCase {

    Integer positiveNum;
    Integer negativeNum;
    Integer exp;

    public void testPositiveNum() {
        positiveNum = 100_000;
        exp = 100_001;
        assertEquals(exp, Zero2OneChanger.fromInteger(positiveNum));
    }

    public void testNegativeNum() {
        negativeNum = -333_004;
        exp = -333_004;
        assertEquals(exp, Zero2OneChanger.fromInteger(negativeNum));
    }

    public void testNull() {
        assertNull(Zero2OneChanger.fromInteger(null));
    }
}
