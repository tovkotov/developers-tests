package count_digits;

import java.util.TreeMap;

public class CountDigits {
    public static TreeMap<Integer, Integer> fromArray(int[] array) {
        TreeMap<Integer, Integer> counts = new TreeMap<>();
        for (int j : array) {
            if (counts.containsKey(j))
                counts.put(j, counts.get(j) + 1);
            else
                counts.put(j, 1);
        }
        return counts;
    }
}
