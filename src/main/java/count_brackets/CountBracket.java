package count_brackets;

import java.util.Deque;
import java.util.LinkedList;

public class CountBracket {

    public static boolean countBracketsCheck(String value) {
        if (value.length() % 2 != 0) return false;
        Deque<String> brackets = new LinkedList<>();

        for (int i = 0; i < value.length(); i++) {
            String bracket = value.substring(i, i + 1);

            if (bracket.equals("(")) {
                brackets.add(bracket);
            }

            if (bracket.equals("[")) {
                brackets.add(bracket);
            }

            if (bracket.equals("]")) {
                if (brackets.getLast().equals("[")) {
                    brackets.removeLast();
                }
            }

            if (bracket.equals(")")) {
                if (brackets.getLast().equals("(")) {
                    brackets.removeLast();
                }
            }
        }

        return brackets.isEmpty();
    }

}
