CREATE TABLE `test`
(
    `val` int NOT NULL
);

insert into test (val)
values ('1'),
       ('2'),
       ('4'),
       ('7'),
       ('8'),
       ('11');

select val+1, l-val-1
from
    (select val,(select val from test t2 where t2.val > t1.val order by val limit 1) l
     from test t1)t
where val < l-1
