package zero_to_one;

public class Zero2OneChanger {
    public static Integer fromInteger(Integer value) {
        if (value != null)
            return value % 10 == 0 ? value + 1 : value;
        return null;
    }
}